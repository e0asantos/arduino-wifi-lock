// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'unlockController'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html',
          controller: 'settingsController'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/search');
})
 .directive('ionPinch', function($timeout,$http) {
  return {
    restrict: 'A',
    link: function($scope, $element) {

      $timeout(function() {
        var square = $element[0],
            posX = 0,
            posY = 0,
            lastPosX = 0,
            lastPosY = 0,
            bufferX = 0,
            bufferY = 0,
            scale = 1,
            lastScale,
            rotation = 0,
            last_rotation, dragReady = 0;
        ionic.onGesture('touch drag transform dragend', function(e) {
          $(".bubble").addClass("expand");
          e.gesture.srcEvent.preventDefault();
          e.gesture.preventDefault();
          switch (e.type) {
            case 'touch':
              lastScale = scale;
              last_rotation = rotation;
              break;
            case 'drag':
            if (window.initPos[0]==0) {
              window.initPos[0]=e.gesture.deltaX + lastPosX;
              window.initPos[1]=e.gesture.deltaY + lastPosY;
            }
              posX = e.gesture.deltaX + lastPosX;
              posY = e.gesture.deltaY + lastPosY;
              

    
              break;
            case 'transform':
              rotation = e.gesture.rotation + last_rotation;
              scale = e.gesture.scale * lastScale
              break;
            case 'dragend':
              lastPosX = 0;
              lastPosY = 0;
              lastScale = scale;
              if (posX < -230 || posX > 151 || posY < -224 || posY > 169) {
                // Unlock
                $(".fa-check").addClass("return");
                $(".fa-lock").addClass("away");
                $(".lock").addClass("good");
                $http({
                  method: 'GET',
                  url: "http://"+window.localStorage['arduinoIP']+'/unlock'
                  }).then(function successCallback(response) {
                      
                    }, function errorCallback(response) {
                      console.log(response);
                    });
              } else {
                posX=window.initPos[0];
                posY=window.initPos[1];
              }
              $(".bubble").removeClass("expand");  
              
              
              break;
          }
          var transform =
              "translate3d(" + posX + "px," + posY + "px, 0) " +
              "scale(" + scale + ")" +
              "rotate(" + rotation + "deg) ";
          e.target.style.transform = transform;
          e.target.style.webkitTransform = transform;
        }, $element[0]);
      });
    }
  };
});
window.initPos=[0,0];