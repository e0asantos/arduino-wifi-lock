angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})
.controller('settingsController', function($scope) {
  $scope.arduino=new Object();
  $scope.arduino.IP="127.0.0.1"
  $scope.retrieveStoredIP=function(){
    if(window.localStorage['arduinoIP']!=undefined && window.localStorage['arduinoIP']!=""){
      $scope.arduino.IP=window.localStorage['arduinoIP']; 
    }
  }
  $scope.saveArduinoIP=function(){
    window.localStorage['arduinoIP']=$scope.arduino.IP; 
  }
  $scope.retrieveStoredIP();
})
.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})
.controller('unlockController', function($scope,$http) {
  //sss
  
    //   var hammertime = Hammer($(".fa-lock").get(0), {
    //     drag: true,
    //     transform: false
    // });
      // Hammer($(".fa-lock").get(0)).on('dragstart', function(event) {
      //   console.log('dragstart', event);
      // });

      // Hammer($(".fa-lock").get(0)).on('drag', function(event){
      //   // console.log('drag', event.gesture.deltaX, event.gesture.deltaY)
      //   var target = event.target;
      //   $(target).css({
      //     'transform': 'translate(' + event.gesture.deltaX + 'px,' + event.gesture.deltaY + 'px)'
      //   });
      // });

      // Hammer(document.body).on('release', function(event){
      //   console.log('release', event);
      //   event.gesture.preventDefault()
      // });
      //var myGesture = ionic.onGesture(type, callback, element, options);
      // ionic.onGesture("drag", $scope.handleHammer, $(".fa-lock").get(0), { drag_lock_to_axis: true });
      // $scope.handleHammer=function(evento){
      //   console.log("callback");
      // }
      // $http({
      //   method: 'GET',
      //   url: "http://"+window.localStorage['arduinoIP']+'/unlock'
      //   }).then(function successCallback(response) {
            
      //     }, function errorCallback(response) {
      //       console.log(response);
      //     });
    
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
